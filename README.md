# Introduction

 This simulator is designed as a teaching and testing tool for adaptive
 neurostimulation research and epilepsy therapy.
 
 The simulation faithfully emulates the dynamics of the rat hippocampal
 brain slice recorded in the Entorhinal Cortex under external electrical  
 stimulation applied to the Amygdala. Key properties of this domain are:  
 nonlinear dynamics, partial observability, noisy observations, and
 currently unknown first principles. An example of typical simulation
 output (without stimulation) is depicted below.

 ![](./signal.png)
 
# Technical Details

## Observation Space

 The system is observable through one continuous variable and one
 discrete variable.
 
1.  observed neural field potential in millivolts (mV)
2.  patient state:
     -   0 for normal
     -   1 for seizure

## Action Space

Neurostim receives a single, discrete action from the set {0,1}. The
action (1) simulates application of a single electrical pulse to the
neural system. The action (0) simulates no stimulation. 

## Reward

Reward values can be modified via messaging. Suggested rewards:
 -   0 per normal patient state
 -   -40 per seizure patient state
 -   -1 per stimulation

# Additional technical details

-   For further details see *Bush, K., Pineau, J., Avoli, M., [Manifold
     Embeddings for Model-Based Reinforcement Learning of
     Neurostimulation Policies](http://www.cs.mcgill.ca/~jpineau/files/kbush-icmlws09.pdf), ICML Workshop on Abstraction in
     Reinforcment Learning, June, 2009.*

## Source

Both source includes:

1.  the Neurostim environment
2.  a fixed frequency stimulating agent
3.  a simple experiment

